

![Ami Engineer](/notes-for-life/img/Ami_Engineer.jpeg)


* In the face of adversity, be brave, stand tall and never shy away from responsibility.
* When buying something, never compromise on quality. Especially if the quality is worth the extra price.
* Always face your fears boldly, Never hide behind anything, be honest to yourself and to other around you.
* Once you make someone your friend, always stand by them. No matter what.
* Never shy away from learning anything from anyone. Specially if they are younger than you.