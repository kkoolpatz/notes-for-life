breadcrumbs: False

![Tree of Knowledge](/notes-for-life/img/tok2.png)

## What is Notes for Life

Hello and welcome to Notes for Life. Notes for Life is a collection of notes, text, videos, quotes and other advise that I came accross in life. I have done my best to read and understand what different people had to say, and if it was something that resonated well with me. i documented it here. 

## Learning is free

I am a strong believer in free learning. Everything on this site is free for you to read and make it yours. If you like what is written here and it resonated with you, feel free to pass it along to someone else. Lets make our world a better place. If you this content helped you, please reach out to me. I'd love to hear your experience. do include what parts you liked, what you didnt, what made sense, what didnt, any feedback good or bad is appreciated. 

## Table of content

**Life**

**Health**

**Education**

**Career**

**Relatonships**

