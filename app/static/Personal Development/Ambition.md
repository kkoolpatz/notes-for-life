title: How to Achieve Your Most Ambitious Goals
youtube: https://www.youtube.com/embed/TQMbvJNRpLE


## Introduction

What stands between us and achieving our most ambitious dreams has far less to do with possessing extraordinary skills or talent, but far more to do with how you approach everyday problems and make decisions to solve those problems. 

It is the continuous and compounding nature of all those millions of decisions that we face on a regular basis, even a marginal improvement in our process can have a huge impact on the end result. 


## Novak Djokovic example

----
Year         | '04-'05 | '06-'07 | '07-'11 
------------ | ------- | ------- |--------
Rank         | 100+    | 3       | 1
Prize Money  | $0.3m   | $5m     | $14m
Matches won  | 49%     | 79%     | 90% 


* Fear of failure
* Self doubt
* Obstacles
* Lack of Motivation
* Laziness

## Conclusion? 

* Lost Opportunities
* A wonderful idea never get executed
* You never realise your potential

## How to program your mind for success

**It doesnt matter if you have no idea how to go about your goal**

* What is important is to take one step at a time, in the direction of your goal. 
* Break down your problems into smaller tasks, then handle one task at a time. 
* When you are stuck, ask for help. 

**You have the power to take control of your thoughts**

* Consciously observe your thoughts. Are you controlling them or are they controlling you?
* Replace negative thoughts with positive thoughts that empower you and help you move towards your goal

**Guided Visualization and Affirmation**

* Guided visualization is a technique, where you close your eyes and vividly picture a visual where you see yourself having already reached your goals.
* You have to feel yourself as if it has already happened.
* Imagine every item on your bucket list completed. The "how" does not matter. The "what" matters.

**Are your goals crazy enough**

* **DREAM BIG!!!** if it doesnt sound **CRAZY** impossible, it is not big enough.
* Do not worry aout how it is going to happen. It is important to believe it has already happened, as you get yourself used to the new reality in which you have already made it big. 

**Success is no accident** 

* You dont just get lucky. Success is not an accident. 
* You have to do in on purpose.

**And It starts with...**

* Knowing exactly what you want to achieve.
* Knowing why you need to achieve it. 
* Knowing what it really means to you.
* Knowing the kind of a person that you need to become to achieve it 
* Programming your mind to achieve it (Visualization, Affirmation )