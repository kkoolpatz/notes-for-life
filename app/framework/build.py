from framework import md_to_html
import os

config = {
    "md_path": "/home/phardikar/git/notes-for-life/app/static",
    "html_path": "/home/phardikar/git/notes-for-life/app/html",
    "template_path": "/home/phardikar/git/notes-for-life/app/templates",
    "page_title": "Notes for Life | Patanjali Hardikar",
    "footer_text": "© Patanjali S. Hardikar",
    "site_title": "Notes for Life",
}


sitemap = list(os.walk(config["md_path"]))
md_to_html(sitemap, config)