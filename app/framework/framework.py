import os
import markdown
import shutil

file_data = {}

def read_md_file(path):
	# file_data[path] = {"level": (9 - len(path.split("/")))* '../'}
	file_data[path] = {"level": "/notes-for-life"}
	try:
		with open(f'{path}', 'r') as file:
			data = file.readlines()
			if not data:
    				data = ['','**This Page is still under construction.**']
			file_data[path]["lines"]= data
	except OSError as e:
		file_data[path]["lines"] = []
		raise
	
	return data

def get_metadata(path, config):
	basename = path.split("/")[-1][:-3]
	metadata = {}
	try:
		file = read_md_file(path)
		
		for line in file:
			if line == '\n':
				break

			if ':' in line:
				key, value = line.rstrip().split(':',1)
				metadata[key] = value.strip()

		metadata["len"] = len(metadata)
		
		# Convert Tags to list		
		if not 'breadcrumbs' in metadata:
			metadata['breadcrumbs'] = "True"
		if 'tags' in metadata:
			metadata['tags'] = metadata['tags'].replace(' ','').split(',')


	except OSError as e:
		if path.split("/")[-1] == 'index.md':
			metadata = {
				'breadcrumbs': "True",
				'len': 0
			}
		else:
			print(f"This file doesnt exist {path}. Error {e}")
			raise

	metadata['title'] = get_title(metadata, basename, config)
	file_data[path]["metadata"] = metadata	
	return metadata

def process_md_file(path, config):
	metadata = get_metadata(path, config)
	try:
		file = file_data[path]["lines"]
	except OSError as e:
		if path.split("/")[-1] == 'index.md':
			return (metadata,"")
		print(f"This file doesnt exist {path}. Error {e}")
		raise

	content = markdown.markdown(''.join(file[metadata["len"]:]), extensions=['tables'])
	file_data[path]["content"] = content
	return (metadata, content)

def md_to_html(sitemap, config):
	html_path = config["html_path"]
	md_path = config["md_path"]
	template_path = config["template_path"]

	# Remove html dir
	try:
		shutil.rmtree(html_path)
	except FileNotFoundError:
		pass
	
	with open(template_path + '/base.html', 'r') as f:
		html_template = ''.join(f.readlines())

	for i in range(len(sitemap)):
		entry = sitemap[i]
		files = entry[2]
		for file in set(files + ['index.md']):
			(metadata, content) = process_md_file(f"{entry[0]}/{file}", config)
			
	print("===============================================")
	for i in list(file_data.keys()):
    		print(i)
	print("===============================================")

	for i in range(len(sitemap)):
		entry = sitemap[i]
		current_dir = entry[0].replace(f"{md_path}","",1)
		if current_dir.startswith('/'): 
			current_dir = current_dir[1:]
		files = entry[2]
		abspath = f"{html_path}/{current_dir}/"
		os.makedirs(abspath, mode = 0o755, exist_ok = True)
		for file in set(files + ['index.md']):
			data_dict = file_data[f"{entry[0]}/{file}"]
			metadata = data_dict["metadata"]
			content = data_dict["content"]
			level = data_dict["level"]
			breadcrumbs = get_breadcrumbs(metadata, current_dir, level, config)
			youtube = get_youtube(metadata, template_path)

			if file == 'index.md':
				toc = build_toc(list(os.walk(f"{md_path}/{current_dir}")), config)
				content = content + toc
				
			with open(f"{html_path}/{current_dir}/{file[:-3]}.html", 'w') as f:
				f.write(html_template.format(
					content=content,
					title=metadata['title'],
					config=config,
					breadcrumbs=breadcrumbs,
					youtube=youtube,
					level=level
				))

def build_toc(sitemap, config):
	md_path = config["md_path"]
	start_dir = sitemap[0][0]
	toc = ""

	for entry in sitemap:

		current_dir = entry[0].replace(f"{md_path}","").replace("//","/")
		if entry[0] != md_path:
			category_text = current_dir.split("/")[-1]
			toc = toc + f"\n\n**[{category_text}](/notes-for-life/html/{current_dir})**\n\n"
	
		for file in entry[2]:
			if file != "index.md":
				path = f"{entry[0]}/{file}".replace("//","/")
				title = file_data[path]["metadata"]["title"].replace(f' - {config["page_title"]}',"").replace(config["page_title"], "")
				url = f"/notes-for-life/html/{current_dir}/{file[:-3]}.html".replace("//","/")
				toc = toc + f"* [{title}]({url})\n"
				# toc = toc + f"* {file[:-3]}.html\n"

	return markdown.markdown(toc)


def get_title(metadata, file, config):
	if 'title' in metadata:
		return f"{metadata['title']} - {config['page_title']}"
	return f"{file.title()} - {config['page_title']}"


def get_youtube(metadata, template_path):
	if 'youtube' in metadata:
		yt = ""
		with open(template_path + '/youtube.html', 'r') as f:
			yt = ''.join(f.readlines())
	else:
		yt = ""
	return yt.format(metadata=metadata)


def get_breadcrumbs(metadata,category, level, config):
	crum = metadata.get('breadcrumbs', 'True')
	breadcrumbs = ""
	title = metadata['title'].replace(f" - {config['page_title']}",'').replace(config['page_title'],'')
	with open(config['template_path'] + '/breadcrumbs.html', 'r') as f:
		bc = ''.join(f.readlines())
	bc_list = category.split("/") + [title]
	while '' in bc_list:
		bc_list.remove('')
	if crum == "True":
		if bc_list:
			path = "/notes-for-life/html"
			for item in bc_list[:-1]:
				path = path + f"/{item}"
				breadcrumbs = f"""<li><a href="{path}" title="{item}">{item}</a></li>"""

			breadcrumbs = breadcrumbs + f"""<li class="active">{bc_list[-1]}</li>"""
			return bc.format(breadcrumbs=breadcrumbs, level=level)
	return ''
		